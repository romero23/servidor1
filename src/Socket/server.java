/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Socket;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import util.ValidacionNumeros;

/**
 *
 * @author romero23
 * 
 */
public class server {

    private ServerSocket serverSocket;
    private Socket cliente;
    private String recivido;
    private DataInputStream dataInputStream;
    private DataOutputStream mensaje;
    private BufferedReader bf;

    public void InitServer() {
        try {
            serverSocket = new ServerSocket(4400);
            while (true) {
                cliente = serverSocket.accept();
                System.out.println("Cliente conectado");
                ValidacionNumeros validacion = new ValidacionNumeros();
                mensaje = new DataOutputStream(cliente.getOutputStream());
                dataInputStream = new DataInputStream(cliente.getInputStream());
              //  bf = new BufferedReader(new InputStreamReader(cliente.getInputStream()));
                recivido = dataInputStream.readUTF();
                
                if (validacion.isInt(recivido)) {
                    System.out.println("Peticion: Listar Archivos");
                    String sDirectorio = "./";
                    String listadoFicheros = "";
                    File f = new File(sDirectorio);
                    File[] ficheros = f.listFiles();

                    for (int x = 0; x < ficheros.length; x++) {
                        if (listadoFicheros.equals("")) {
                            listadoFicheros = ficheros[x].getName();
                        } else {
                            listadoFicheros = listadoFicheros + ";" + ficheros[x].getName();
                        }

                    }
                    mensaje.writeUTF(listadoFicheros);//<- "directorio1;directorio2;directorio3"
 
                } else {
                    System.out.println("Peticion de archivo: " + recivido);
                    String ip = cliente.getInetAddress().toString();
                    String replaceAll = ip.replaceAll("/", "");
                    enviarArchivo("./" + recivido,replaceAll );
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(server.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void enviarArchivo(String nombreArchivo, String ip) {
        try {
            // Creamos la direccion IP de la maquina que recibira el archivo
            InetAddress direccion = InetAddress.getByName(ip);

            // Creamos el Socket con la direccion y elpuerto de comunicacion
            Socket socket = new Socket(direccion, 4403);
            socket.setSoTimeout(2000);
            socket.setKeepAlive(true);

            // Creamos el archivo que vamos a enviar
            File archivo = new File(nombreArchivo);

            // Obtenemos el tamaño del archivo
            int tamañoArchivo = (int) archivo.length();

            // Creamos el flujo de salida, este tipo de flujo nos permite 
            // hacer la escritura de diferentes tipos de datos tales como
            // Strings, boolean, caracteres y la familia de enteros, etc.
            DataOutputStream dos = new DataOutputStream(socket.getOutputStream());

            System.out.println("Enviando Archivo: " + archivo.getName());

            // Enviamos el nombre del archivo 
            dos.writeUTF(archivo.getName());

            // Enviamos el tamaño del archivo
            dos.writeInt(tamañoArchivo);

            // Creamos flujo de entrada para realizar la lectura del archivo en bytes
            FileInputStream fis = new FileInputStream(nombreArchivo);
            BufferedInputStream bis = new BufferedInputStream(fis);

            // Creamos el flujo de salida para enviar los datos del archivo en bytes
            BufferedOutputStream bos = new BufferedOutputStream(socket.getOutputStream());

            // Creamos un array de tipo byte con el tamaño del archivo 
            byte[] buffer = new byte[tamañoArchivo];

            // Leemos el archivo y lo introducimos en el array de bytes 
            bis.read(buffer);

            // Realizamos el envio de los bytes que conforman el archivo
            for (int i = 0; i < buffer.length; i++) {
                bos.write(buffer[i]);
            }

            System.out.println("Archivo Enviado: " + archivo.getName());
            // Cerramos socket y flujos
            bis.close();
            bos.close();
            socket.close();
        } catch (IOException e) {
            System.out.println(e.toString());
        }

    }

}
